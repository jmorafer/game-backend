FROM openjdk:11
ADD ./target/gamebackend-0.0.1-SNAPSHOT.jar /usr/src/app.jar
WORKDIR usr/src
ENTRYPOINT ["java","-jar", "app.jar"]
