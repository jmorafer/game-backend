# Game Backend
Demo project for Zoovu Game 

# Running the app 
1. Build the jar `mvn package`
2. Run with docker `docker-compose up`
# Features 

# Authentication
This app uses bearer token authorization, all requests requiere the`Authorization` header in order to obtain the user doing the request.

The public APIs are:

- Auth login
- Auth sign up
- Auth verify account (Verify email used on sign up)
- User check username availability (Used when the user is filling the sign up from on the front end)
- User check email availability  (Used when the user is filling the sign up from on the front end)

# Leaderboard
In order to offer high scalability an hybrid between caching, storing in `redis`, and retrieving directly from `MySQL` has been implemented.

There are two different flows

- `Today`, `Month` and `Year` leaderboards flow
- `All time` leaderboard flow

## Today, month and year

### Register 
When a game sesion finishes, the following steps occur:

1. Update the `Game` object in `MySQL`
2. Add an object `{ gameId }` to the ZSET (`leaderboardYear`) sorting by the field `score`
3. Add an object `{ gameId }` to the ZSET (`leaderboardWeek`) sorting by the field `score`
4. Add an object `{ gameId }` to the ZSET (`leaderboardToday`) sorting by the field `score`
5. Add an object `{ gameId, username, score }` to the HMAP (`leaderboardValue`)

### Lookup
1. Get the best N game ID's by score in the leaderboard ZSET (`leaderboardYear`, `leaderboardWeek` or `leaderboardToday`)
2. Get the info associated to each ID in the leaderboardValue HMAP (`leaderboardValue`)
3. Return the obtained info the API consumer

### Flush 
In order to optimize the memory usage, the ZSETS  `leaderboardYear` `leaderboardWeek` `leaderboardToday` `leaderboardValue` are flushed by scheduled jobs defined on `com.example.gamebackend.scheduled.LeaderboardFlushJob`

- Each start of day flush all values in `leaderboardToday`
- Each start of week flush all values in `leaderboardWeek`
- Each start of year we would need to flush all values in `leaderboardYear`. However, when flushing `leaderboardYear` some values of `leaderboardValue` should be removed as well since they won't be used on the new year:
    - Retrieve all ID's present on `leaderboardYear`
    - Delete all values in `leaderboardValue` which correspond the ID's obtained on the previous step
    - Flush all values in `leaderboardYear`

## All time
Because this leaderboard type takes into consideration all games, it wouldn't make sense to store it on `redis`, otherwise we would be storing a complete copy of the data that is already in `MySQL`.


However, this being a leaderboard query feature there still needs to be some optimization in order to 
offer low response times, and the capability to handle a large amount of requests. The solution that should be implemented is `caching`, each request to the `ALL_TIME Leaderboard` should cached for 5 minutes, however it is not covered in this demo project. 


# Future features

## Multiplayer!
In order to support multiplayer the following changes would be needed 

### New Join Game Session endpoint

Request
```json
{
    "sessionUuid": "uuid"
}
```

Response
```js
Joined!
```

This endpoint will do the following process:

1. Obtain the authenticated user from the Authorization header
2. Search a `Session` on db with the specified uuid and `ON_HOLD` status
3. If no `Session` is found, throw new `Resource not found`
4. If a `Session` is found, create a new object `SessionParticipant` and persist it
5. Return a success response 

### Modify query for All Time leaderboard
Since now a `Session` can have multiple participants, the query should be modified to lookup the new `SessionParticipant` table

```sql
select (...) from session_particpant left join session (...) order by session.score asc top 100
```
### Add information to Save Mistakes API
The SessionMistake should include a new field `User` that indicates who did the mistake, it will be automatically obtained from the bearer token on the `Authorization` header

## Realtime
Since we are supporting multiplayer now, in order to improve the QoL of our users the next step should be to enable real time interaction between the players of the same session.

Realtime processing screams for WebSockets, each player will be subscribed to a WebSocket channel identified by the game UUID when joining.
Also, we will now register more actions such as `Placing a card`, `Grabbing a card` `User mouse position`

Message examples
```json
{
  "type": "PLACE_CARD_SUCCESS",
  "user": "jhondoe"
}
```
```json
{
  "type": "USER_MOUSE_MOVEMENT",
  "user": "jhondoe",
  "x": 10,
  "y": 20,
  "maxX": 1000,
  "maxY": 1000
}
```

All the game logic will be processed on the server, the database will still be used to persist data created in the game session.

Finally, the Sessions API will be used as a fallback in case the user loses connection to the WebSocket.

## Reconnect
New  API endpoint used to retrieve an active game session uuid from an user.

Request 
```json
{}
```

Response
```json
{
  "sessionUuuid": "uuid"
}  
```

The API should do:

1. Obtain the user from the `Authorization` header
2. Lookup an active session for the desired user `select (...) from session_participant left join session (...) where session_participant.id_user = ? and session.status = 'RUNNING' `
3. Throw `ResourceNotFoundException` if no game is found
4. Otherwise, return the game UUID

## Leaderboard by country
Most games offer the possibility to filter the leaderboard by the user's country. 

In order to support this feature the follow changes would be needed:

- Add an optional parameter `country` to `/api/leaderboard`. This parameter would contain the ISO code of the desired country.
- The leaderboard registration flow would need to include a step to create 4 additional inserts to ZSETS by country. 
For instance, if the user's country is Peru, each time a `GameResult` is created, related info will be 
inserted in the ZSETS `leaderboardYearPE` `leaderboardWeekPE` `leaderboardTodayPE` `leaderboardYear` 
`leaderboardWeek` `leaderboardToday` `leaderboardValue`
- The leaderboard lookup flow would need to search on the new ZSETS if the parameter `country` is present

## Make competition more fair 
Since we are supporting multiplayer now, we should define different queue types (3 players, 5 players, solo)
The leaderboard should be filtered by these types as well, otherwise it wouldn't be fair to the lonely wolfs out there.

New request Leaderboard query API
```json
{
  "periodType": "WEEK",
  "queueType": "5P"
}
```

New request Leaderboard all time API
```json
{
  "queueType": "5P"
}
```



New response Leaderboard API
```json
{
  "users": [ 
    { "username":  "jhondoe", "name":  "John Doe"},  
    { "username":  "jhondoe2", "name":  "John Doe2"}  
  ],
  "score": 1000,
  "obtainedAt": "iso-date"
}
```

## Reviewing 
A Postman collection is provided in the repo `app_collection.json`, also you can check the swagger generated at `http://localhost:8080/swagger-ui/`

## Techologies used
- SpringBoot (Security, data-jpa, data-redis)
- SpringFox
- MySQL@5.7
- Redis@latest