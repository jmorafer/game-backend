package com.example.gamebackend.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.gamebackend.api.dto.session.SessionSaveMistakeRequest;
import com.example.gamebackend.config.AppProperties;
import com.example.gamebackend.model.LeaderboardItem;
import com.example.gamebackend.model.Session;
import com.example.gamebackend.model.SessionMistake;
import com.example.gamebackend.model.SessionStatus;
import com.example.gamebackend.repository.SessionMistakeRepository;
import com.example.gamebackend.repository.SessionRepository;
import com.example.gamebackend.service.impl.SessionServiceImpl;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class SessionServiceTest {

  @Mock
  private SessionMistakeRepository sessionMistakeRepository;

  @Mock
  private AppProperties appProperties;

  @Mock
  private RedisTemplate<String, Long> redisLeaderboardScore;

  @Mock
  private RedisTemplate<String, LeaderboardItem> redisLeaderboardValue;

  @Mock
  private SessionRepository repository;

  @InjectMocks
  private SessionServiceImpl service;

  @Test
  public void saveMistakeShouldFetchSession() {
    when(repository.findByUuidAndStatus(ArgumentMatchers.eq("uuid"),
        ArgumentMatchers.eq(SessionStatus.RUNNING)))
        .thenReturn(Optional.of(Session.builder().id(1L).uuid("uuid").build()));

    service.saveMistake(SessionSaveMistakeRequest.builder().sessionUuid("uuid").build());

    verify(repository, times(1)).findByUuidAndStatus(ArgumentMatchers.eq("uuid"),
        ArgumentMatchers.eq(SessionStatus.RUNNING));
  }

  @Test
  public void saveMistakeShouldPersistMistake() {
    when(repository.findByUuidAndStatus(ArgumentMatchers.eq("uuid"),
        ArgumentMatchers.eq(SessionStatus.RUNNING)))
        .thenReturn(Optional.of(Session.builder().id(1L).uuid("uuid").build()));

    service.saveMistake(SessionSaveMistakeRequest.builder().sessionUuid("uuid").build());

    verify(sessionMistakeRepository, times(1)).save(ArgumentMatchers.any(SessionMistake.class));
  }

}