package com.example.gamebackend.security;

import com.example.gamebackend.model.User;
import com.example.gamebackend.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

  private UsersRepository usersRepository;

  @Override
  public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
    return UserPrincipal.fromUser(
        usersRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
            .orElseThrow(() -> new RuntimeException()));
  }

  @Transactional(readOnly = true)
  public UserDetails loadUserById(Long id) {
    User user = usersRepository.findById(id).orElseThrow(
        () -> new RuntimeException()
    );

    return UserPrincipal.fromUser(user);
  }

}
