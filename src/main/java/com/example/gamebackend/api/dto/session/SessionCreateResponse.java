package com.example.gamebackend.api.dto.session;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessionCreateResponse {

  @ApiModelProperty(value = "Game session's UUID")
  private String uuid;
  
}
