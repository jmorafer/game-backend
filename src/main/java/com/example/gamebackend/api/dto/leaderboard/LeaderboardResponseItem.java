package com.example.gamebackend.api.dto.leaderboard;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LeaderboardResponseItem {

  @ApiModelProperty(example = "johndoe")
  private String username;

  @ApiModelProperty(example = "123")
  private Long score;

  @ApiModelProperty(example = "2020-10-05T06:59:08.622Z")
  private String obtainedAt;

}
