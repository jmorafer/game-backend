package com.example.gamebackend.api.dto.session;

import lombok.Data;

@Data
public class SessionStartRequest {

  private String sessionUuid;

}
