package com.example.gamebackend.api.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserMeResponse {

  @ApiModelProperty(example = "1")
  private Long id;

  @ApiModelProperty(value = "My username", example = "johndoe")
  private String username;

  @ApiModelProperty(value = "My name", example = "John Doe")
  private String name;


}
