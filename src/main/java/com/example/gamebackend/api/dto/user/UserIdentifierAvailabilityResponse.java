package com.example.gamebackend.api.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserIdentifierAvailabilityResponse {

  @ApiModelProperty(
      value = "Flag that indicates whether the identifier is available or not",
      example = "true")
  private Boolean available;

}
