package com.example.gamebackend.api.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthLoginResponse {

  @ApiModelProperty(
      value = "Bearer token to be sent in future requests",
      example = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI0IiwiaWF0IjoxNjAxODU5MDIyLCJleHAiOjE2MDE4NjA4MjJ9.7331v8xoARe-ZdsGraL1uDwE-205N_XQ8daCa7J3kZpy1XJ8be-mB3X_Ux6QHoReSlA_xycVbmfVWDcdbMPpWA")
  private String accessToken;

}
