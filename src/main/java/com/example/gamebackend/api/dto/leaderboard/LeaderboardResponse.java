package com.example.gamebackend.api.dto.leaderboard;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LeaderboardResponse {

  @ApiModelProperty(value = "Sorted list of scores from best to worst")
  private List<LeaderboardResponseItem> list;

}
