package com.example.gamebackend.api.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthSignUpRequest {

  @NotBlank
  @ApiModelProperty(value = "Username, must be unique", example = "user")
  private String username;

  @Email
  @ApiModelProperty(value = "Email the user has access to, will be verified. Must be unique", example = "user@mail.com")
  private String email;

  @NotBlank
  @ApiModelProperty(value = "Text plain password", example = "password")
  private String password;

  @NotBlank
  @ApiModelProperty(value = "Human readable name for user", example = "John Doe")
  private String name;

}
