package com.example.gamebackend.api.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthLoginRequest {

  @ApiModelProperty(
      value = "Username or email of the user trying to login",
      example = "jhondoe@email.com")
  private String usernameOrEmail;

  @ApiModelProperty(
      value = "Password of the user trying to login",
      example = "password")
  private String password;

}
