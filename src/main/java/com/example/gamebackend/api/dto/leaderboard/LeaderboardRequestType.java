package com.example.gamebackend.api.dto.leaderboard;

import java.util.HashMap;
import java.util.Map;

public enum LeaderboardRequestType {
  YEAR, WEEK, DAY;

  private static final Map<String, LeaderboardRequestType> map;

  static {
    map = new HashMap<>();
    for (LeaderboardRequestType type : LeaderboardRequestType.values()) {
      map.put(type.name(), type);
    }
  }

  public static LeaderboardRequestType findByValue(String value) {
    return map.get(value);
  }

}
