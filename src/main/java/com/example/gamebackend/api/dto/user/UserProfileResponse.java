package com.example.gamebackend.api.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileResponse {

  @ApiModelProperty(name = "User's username", example = "johndoe")
  private String username;

  @ApiModelProperty(name = "User's name", example = "John Doe")
  private String name;

}
