package com.example.gamebackend.api.error;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -6438023345095165123L;

  public ResourceNotFoundException(String resourceName, String field, String value) {
    super(String.format("Resource [%s] with [%s]=[%s] not found", resourceName, field, value));
  }

}
