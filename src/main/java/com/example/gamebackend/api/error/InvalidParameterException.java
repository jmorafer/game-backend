package com.example.gamebackend.api.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidParameterException extends RuntimeException {

  private static final long serialVersionUID = 5826106073664865530L;

  public InvalidParameterException(String parameter, String value) {
    super(String.format("Parameter [%s]=[%s] is not valid", parameter, value));
  }

}
