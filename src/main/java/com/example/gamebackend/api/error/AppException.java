package com.example.gamebackend.api.error;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class AppException extends RuntimeException {

  private static final long serialVersionUID = 7607213619672648782L;

  public AppException() {
    super();
  }

  public AppException(String message, Throwable cause) {
    super(message, cause);
  }

  public AppException(String message) {
    super(message);
  }

  public AppException(Throwable cause) {
    super(cause);
  }

}
