package com.example.gamebackend.model;

import java.io.Serializable;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LeaderboardItem implements Serializable {

  private static final long serialVersionUID = 2586706217877833697L;

  private Long id;

  private String username;

  private Long score;

  private Instant obtainedAt;

}
