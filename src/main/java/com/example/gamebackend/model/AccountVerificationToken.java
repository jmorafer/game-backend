package com.example.gamebackend.model;


import java.io.Serializable;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountVerificationToken implements Serializable {

  public static final String CACHE_PREFIX = "accountVerificationToken";

  private static final long serialVersionUID = -4334325921054573559L;

  @Id
  private String username;

  private String uuid;

}
