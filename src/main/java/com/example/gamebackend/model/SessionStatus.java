package com.example.gamebackend.model;

public enum SessionStatus {
  ON_HOLD, RUNNING, FINALIZED, ABORTED;
}
