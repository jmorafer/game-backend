package com.example.gamebackend.mail;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class MailService {

  private final JavaMailSender mailSender;

  public void sendTextMail(MailInstance instance) {
    MimeMessagePreparator mimeMessagePreparator = mimeMessage -> {
      MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
      mimeMessageHelper.setFrom("no-reply@zoovu.com");
      mimeMessageHelper.setTo(instance.getTo());
      mimeMessageHelper.setSubject(instance.getSubject());
      mimeMessageHelper.setText(instance.getText());
    };

    try {
      mailSender.send(mimeMessagePreparator);
      log.info("Sent email {}", instance);
    } catch (MailException e) {
      throw new RuntimeException(e);
    }
  }

}
