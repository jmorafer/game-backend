package com.example.gamebackend.repository;

import com.example.gamebackend.model.Session;
import com.example.gamebackend.model.SessionMistake;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionMistakeRepository extends JpaRepository<SessionMistake, Long> {

  Long countAllBySession(Session session);

}
