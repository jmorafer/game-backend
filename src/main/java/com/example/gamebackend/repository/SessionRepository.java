package com.example.gamebackend.repository;

import com.example.gamebackend.model.Session;
import com.example.gamebackend.model.SessionStatus;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {

  List<Session> findAllByStatus(SessionStatus status, Pageable page);

  Optional<Session> findByUuidAndStatus(String uuid, SessionStatus status);

}
