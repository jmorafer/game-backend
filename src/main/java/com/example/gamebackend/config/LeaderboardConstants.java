package com.example.gamebackend.config;

public interface LeaderboardConstants {

  String REDIS_TODAY = "leaderboardToday";

  String REDIS_WEEK = "leaderboardWeek";

  String REDIS_YEAR = "leaderboardYear";

  String REDIS_VALUE = "leaderboardValue";

}
