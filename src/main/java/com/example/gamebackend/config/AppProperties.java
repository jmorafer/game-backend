package com.example.gamebackend.config;

import java.time.Duration;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class AppProperties {

  @Value("${app.verificationTokenDuration}")
  private Duration verificationTokenDuration;

  @Value("${app.gameMistakePenalization}")
  private Long gameMistakePenalization;

}
