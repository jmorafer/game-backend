package com.example.gamebackend.service.impl;

import com.example.gamebackend.api.dto.user.UserIdentifierAvailabilityResponse;
import com.example.gamebackend.api.dto.user.UserMeResponse;
import com.example.gamebackend.api.dto.user.UserProfileResponse;
import com.example.gamebackend.api.error.ResourceNotFoundException;
import com.example.gamebackend.model.User;
import com.example.gamebackend.repository.UsersRepository;
import com.example.gamebackend.security.UserPrincipal;
import com.example.gamebackend.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

  private UsersRepository usersRepository;

  @Override
  public UserIdentifierAvailabilityResponse checkAvailabilityUsername(String username) {
    Boolean result = usersRepository.existsByUsername(username);
    return UserIdentifierAvailabilityResponse
        .builder()
        .available(!result)
        .build();
  }

  @Override
  public UserIdentifierAvailabilityResponse checkAvailabilityEmail(String email) {
    Boolean result = usersRepository.existsByEmail(email);
    return UserIdentifierAvailabilityResponse
        .builder()
        .available(!result)
        .build();
  }

  @Override
  public UserProfileResponse searchByUsername(String username) {
    User user = usersRepository.findByUsername(username)
        .orElseThrow(() -> new ResourceNotFoundException("user", "username", username));

    return UserProfileResponse
        .builder()
        .name(user.getName())
        .username(user.getUsername())
        .build();
  }

  @Override
  public UserMeResponse me(UserPrincipal userPrincipal) {
    User user = usersRepository.findById(userPrincipal.getId())
        .orElseThrow(
            () -> new ResourceNotFoundException("user", "id", userPrincipal.getId().toString()));

    return UserMeResponse
        .builder()
        .id(userPrincipal.getId())
        .name(user.getName())
        .username(user.getUsername())
        .build();
  }
}
