package com.example.gamebackend.service.impl;

import com.example.gamebackend.api.dto.session.SessionCreateResponse;
import com.example.gamebackend.api.dto.session.SessionSaveMistakeRequest;
import com.example.gamebackend.api.error.ResourceNotFoundException;
import com.example.gamebackend.config.AppProperties;
import com.example.gamebackend.config.LeaderboardConstants;
import com.example.gamebackend.model.LeaderboardItem;
import com.example.gamebackend.model.Session;
import com.example.gamebackend.model.SessionMistake;
import com.example.gamebackend.model.SessionStatus;
import com.example.gamebackend.model.User;
import com.example.gamebackend.repository.SessionMistakeRepository;
import com.example.gamebackend.repository.SessionRepository;
import com.example.gamebackend.security.UserPrincipal;
import com.example.gamebackend.service.SessionService;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
public class SessionServiceImpl implements SessionService {

  private SessionRepository sessionRepository;
  private SessionMistakeRepository sessionMistakeRepository;
  private AppProperties appProperties;
  private RedisTemplate<String, Long> redisLeaderboardScore;
  private RedisTemplate<String, LeaderboardItem> redisLeaderboardValue;

  @Override
  @Transactional
  public SessionCreateResponse create(UserPrincipal userPrincipal) {
    User user = new User();
    user.setId(userPrincipal.getId());

    Session session = new Session();
    session.setUser(user);
    session.setStatus(SessionStatus.ON_HOLD);
    session.setUuid(UUID.randomUUID().toString());
    sessionRepository.save(session);

    log.info("session-{}: created", session.getUuid());

    return SessionCreateResponse
        .builder()
        .uuid(session.getUuid())
        .build();
  }

  @Override
  @Transactional
  public void start(String gameSessionUuid) {
    Session session = sessionRepository.findByUuidAndStatus(gameSessionUuid, SessionStatus.ON_HOLD)
        .orElseThrow(
            () -> new ResourceNotFoundException("active game session", "uuid", gameSessionUuid));
    session.setStatus(SessionStatus.RUNNING);
    session.setStartedAt(Instant.now());
    sessionRepository.save(session);
    log.info("session-{}: started", gameSessionUuid);
  }

  @Override
  @Transactional
  public void finalize(String gameSessionUuid) {
    Session session = sessionRepository.findByUuidAndStatus(gameSessionUuid, SessionStatus.RUNNING)
        .orElseThrow(
            () -> new ResourceNotFoundException("active game session", "uuid", gameSessionUuid));

    Long countMistakes = sessionMistakeRepository.countAllBySession(session);

    session.setStatus(SessionStatus.FINALIZED);
    session.setEndedAt(Instant.now());

    Long score = Duration.between(session.getStartedAt(), session.getEndedAt()).toSeconds()
        + appProperties.getGameMistakePenalization() * countMistakes;

    session.setScore(score);
    sessionRepository.save(session);

    ZSetOperations<String, Long> opsZset = redisLeaderboardScore.opsForZSet();

    opsZset.add(LeaderboardConstants.REDIS_TODAY, session.getId(), session.getScore());
    opsZset.add(LeaderboardConstants.REDIS_WEEK, session.getId(), session.getScore());
    opsZset.add(LeaderboardConstants.REDIS_YEAR, session.getId(), session.getScore());

    HashOperations<String, Long, LeaderboardItem> hashOps = redisLeaderboardValue.opsForHash();

    hashOps.put(LeaderboardConstants.REDIS_VALUE, session.getId(), LeaderboardItem
        .builder()
        .id(session.getId())
        .score(session.getScore())
        .username(session.getUser().getUsername())
        .obtainedAt(session.getEndedAt())
        .build());

    log.info("session-{}: finalized (score={})", gameSessionUuid, score);
  }

  @Override
  @Transactional
  public void saveMistake(SessionSaveMistakeRequest request) {
    Session session = sessionRepository
        .findByUuidAndStatus(request.getSessionUuid(), SessionStatus.RUNNING)
        .orElseThrow(
            () -> new ResourceNotFoundException("active game session", "uuid",
                request.getSessionUuid()));

    SessionMistake sessionMistake = new SessionMistake();
    sessionMistake.setRecordedAt(Instant.now());
    sessionMistake.setSession(session);
    sessionMistakeRepository.save(sessionMistake);
    log.info("session-{}: recorded mistake", request.getSessionUuid());
  }

}
