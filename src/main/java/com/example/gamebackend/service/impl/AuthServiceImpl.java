package com.example.gamebackend.service.impl;

import com.example.gamebackend.api.dto.auth.AuthLoginRequest;
import com.example.gamebackend.api.dto.auth.AuthLoginResponse;
import com.example.gamebackend.api.dto.auth.AuthSignUpRequest;
import com.example.gamebackend.api.error.AppException;
import com.example.gamebackend.api.error.ResourceNotFoundException;
import com.example.gamebackend.config.AppProperties;
import com.example.gamebackend.mail.MailInstance;
import com.example.gamebackend.mail.MailService;
import com.example.gamebackend.model.AccountVerificationToken;
import com.example.gamebackend.model.User;
import com.example.gamebackend.repository.UsersRepository;
import com.example.gamebackend.security.JwtTokenProvider;
import com.example.gamebackend.service.AuthService;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
public class AuthServiceImpl implements AuthService {

  private AppProperties appProperties;
  private JwtTokenProvider jwtTokenProvider;
  private UsersRepository usersRepository;
  private AuthenticationManager authenticationManager;
  private PasswordEncoder passwordEncoder;
  private RedisTemplate<String, AccountVerificationToken> cache;
  private MailService mailService;

  @Override
  public AuthLoginResponse login(AuthLoginRequest request) {
    Authentication authenticatedUser = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(request.getUsernameOrEmail(),
            request.getPassword())
    );

    SecurityContextHolder.getContext().setAuthentication(authenticatedUser);

    String token = jwtTokenProvider.generateToken(authenticatedUser);

    return AuthLoginResponse
        .builder()
        .accessToken(token)
        .build();
  }

  @Override
  @Transactional
  public void signup(AuthSignUpRequest request) {
    User user = User
        .builder()
        .username(request.getUsername())
        .email(request.getEmail())
        .name(request.getName())
        .verified(Boolean.FALSE)
        .password(passwordEncoder.encode(request.getPassword()))
        .build();

    String uuid = UUID.randomUUID().toString();
    String key = generateVerificationTokenKey(uuid);

    ValueOperations<String, AccountVerificationToken> valueOps = cache.opsForValue();

    AccountVerificationToken token = valueOps.get(key);
    if (token != null) {
      throw new AppException("Duplicated UUID, please try again");
    } else {
      token = AccountVerificationToken
          .builder()
          .username(user.getUsername())
          .uuid(uuid)
          .build();

      log.info("Creating verification token for user {} {}", user, token);
      mailService.sendTextMail(MailInstance
          .builder()
          .subject("Verify your Zoovu Game account!")
          .text(
              String.format(
                  "Please follow the link below to verify your account.\n\nhttp://localhost:8080/api/auth/verify?token=%s",
                  uuid))
          .to(user.getEmail())
          .build());
      valueOps.set(key, token, appProperties.getVerificationTokenDuration());
      usersRepository.save(user);
    }
  }

  @Override
  public void verify(String verificationToken) {
    ValueOperations<String, AccountVerificationToken> valueOps = cache.opsForValue();

    String key = generateVerificationTokenKey(verificationToken);

    AccountVerificationToken token = valueOps.get(key);

    if (token != null) {
      User user = usersRepository.findByUsername(token.getUsername())
          .orElseThrow(
              () -> new ResourceNotFoundException("user", "username", token.getUsername()));
      user.setVerified(Boolean.TRUE);
      usersRepository.save(user);
      cache.delete(key);
    } else {
      throw new RuntimeException("Verification token not found");
    }
  }

  private String generateVerificationTokenKey(String uuid) {
    return String.format("%s:%s", AccountVerificationToken.CACHE_PREFIX, uuid);
  }

}
