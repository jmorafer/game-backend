package com.example.gamebackend.service.impl;

import com.example.gamebackend.api.dto.leaderboard.LeaderboardRequestType;
import com.example.gamebackend.api.dto.leaderboard.LeaderboardResponse;
import com.example.gamebackend.api.dto.leaderboard.LeaderboardResponseItem;
import com.example.gamebackend.config.LeaderboardConstants;
import com.example.gamebackend.model.LeaderboardItem;
import com.example.gamebackend.model.SessionStatus;
import com.example.gamebackend.repository.SessionRepository;
import com.example.gamebackend.service.LeaderboardService;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
public class LeaderboardServiceImpl implements LeaderboardService {

  private RedisTemplate<String, Long> redisLeaderboardScore;
  private SessionRepository sessionRepository;

  @Override
  public LeaderboardResponse query(LeaderboardRequestType requestType) {
    String prefix = null;

    switch (requestType) {
      case DAY:
        prefix = LeaderboardConstants.REDIS_TODAY;
        break;
      case WEEK:
        prefix = LeaderboardConstants.REDIS_WEEK;
        break;
      case YEAR:
        prefix = LeaderboardConstants.REDIS_YEAR;
        break;
    }

    Set<Long> set = redisLeaderboardScore.opsForZSet().reverseRange(prefix, 0L, 100L);

    return LeaderboardResponse
        .builder()
        .list(set
            .stream()
            .map(
                item -> (LeaderboardItem) redisLeaderboardScore.opsForHash()
                    .get(LeaderboardConstants.REDIS_VALUE, item))
            .filter(Objects::nonNull)
            .map(item -> LeaderboardResponseItem
                .builder()
                .username(item.getUsername())
                .score(item.getScore())
                .obtainedAt(
                    item.getObtainedAt() != null
                        ? item.getObtainedAt().atOffset(ZoneOffset.UTC)
                        .format(DateTimeFormatter.ISO_DATE_TIME)
                        : null)
                .build())
            .collect(Collectors.toList()))
        .build();
  }

  @Override
  @Transactional(readOnly = true)
  public LeaderboardResponse queryAllTime() {
    List<LeaderboardResponseItem> list = sessionRepository
        .findAllByStatus(SessionStatus.FINALIZED, PageRequest.of(0, 100, Sort.by("score")))
        .stream()
        .map(item -> LeaderboardResponseItem
            .builder()
            .username(item.getUser().getUsername())
            .score(item.getScore())
            .build())
        .collect(Collectors.toList());

    return LeaderboardResponse
        .builder()
        .list(list)
        .build();

  }

}
