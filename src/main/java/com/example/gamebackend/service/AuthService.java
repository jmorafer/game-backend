package com.example.gamebackend.service;

import com.example.gamebackend.api.dto.auth.AuthLoginRequest;
import com.example.gamebackend.api.dto.auth.AuthLoginResponse;
import com.example.gamebackend.api.dto.auth.AuthSignUpRequest;

public interface AuthService {

  AuthLoginResponse login(AuthLoginRequest request);

  void signup(AuthSignUpRequest request);

  void verify(String verificationToken);

}
