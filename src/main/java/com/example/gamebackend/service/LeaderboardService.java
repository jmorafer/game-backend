package com.example.gamebackend.service;

import com.example.gamebackend.api.dto.leaderboard.LeaderboardRequestType;
import com.example.gamebackend.api.dto.leaderboard.LeaderboardResponse;

public interface LeaderboardService {

  LeaderboardResponse query(LeaderboardRequestType requestType);

  LeaderboardResponse queryAllTime();

}
