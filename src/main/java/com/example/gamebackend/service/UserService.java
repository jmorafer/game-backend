package com.example.gamebackend.service;

import com.example.gamebackend.api.dto.user.UserIdentifierAvailabilityResponse;
import com.example.gamebackend.api.dto.user.UserMeResponse;
import com.example.gamebackend.api.dto.user.UserProfileResponse;
import com.example.gamebackend.security.UserPrincipal;

public interface UserService {

  UserIdentifierAvailabilityResponse checkAvailabilityUsername(String username);

  UserIdentifierAvailabilityResponse checkAvailabilityEmail(String email);

  UserProfileResponse searchByUsername(String username);

  UserMeResponse me(UserPrincipal userPrincipal);
}
