package com.example.gamebackend.service;

import com.example.gamebackend.api.dto.session.SessionCreateResponse;
import com.example.gamebackend.api.dto.session.SessionSaveMistakeRequest;
import com.example.gamebackend.security.UserPrincipal;

public interface SessionService {

  SessionCreateResponse create(UserPrincipal userPrincipal);

  void start(String gameSessionUuid);

  void finalize(String gameSessionUuid);

  void saveMistake(SessionSaveMistakeRequest request);

}
