package com.example.gamebackend.scheduled;

import com.example.gamebackend.config.LeaderboardConstants;
import com.example.gamebackend.model.LeaderboardItem;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class LeaderboardFlushJob {

  private RedisTemplate<String, Long> redisIds;
  private RedisTemplate<String, LeaderboardItem> redisValues;

  @Scheduled(cron = "0 0 1 * * *")
  private void flushTodayScores() {
    redisIds.delete(LeaderboardConstants.REDIS_TODAY);
  }

  @Scheduled(cron = "0 0 1 * * MON")
  private void flushWeekScores() {
    redisIds.delete(LeaderboardConstants.REDIS_WEEK);
  }

  @Scheduled(cron = "0 0 0 1 1 ?")
  private void flushAllYearScores() {
    ZSetOperations<String, Long> zsetOps = redisIds.opsForZSet();
    HashOperations<String, Long, LeaderboardItem> hashOps = redisValues.opsForHash();

    Set<Long> set = zsetOps
        .rangeByScore(LeaderboardConstants.REDIS_YEAR, Double.MIN_VALUE, Double.MAX_VALUE);

    for (Long id : set) {
      hashOps.delete(LeaderboardConstants.REDIS_VALUE, id.toString());
    }

    redisIds.delete(LeaderboardConstants.REDIS_VALUE);
  }

}
