package com.example.gamebackend.controller;

import com.example.gamebackend.api.dto.leaderboard.LeaderboardRequestType;
import com.example.gamebackend.api.dto.leaderboard.LeaderboardResponse;
import com.example.gamebackend.api.error.InvalidParameterException;
import com.example.gamebackend.service.LeaderboardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Leaderboard API")
@RestController
@RequestMapping("/api/leaderboard")
@AllArgsConstructor
public class LeaderboardController {

  private LeaderboardService service;

  @GetMapping
  @ApiOperation(value = "Query")
  private LeaderboardResponse query(
      @ApiParam(allowableValues = "DAY, WEEK, YEAR") @RequestParam String type) {
    LeaderboardRequestType typeEnum = LeaderboardRequestType.findByValue(type);
    if (typeEnum == null) {
      throw new InvalidParameterException("type", type);
    }
    return service.query(typeEnum);
  }

  @GetMapping("/alltime")
  @ApiOperation(value = "Query all time")
  private LeaderboardResponse queryAllTime() {
    return service.queryAllTime();
  }
}