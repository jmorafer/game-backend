package com.example.gamebackend.controller;

import com.example.gamebackend.api.dto.session.SessionCreateResponse;
import com.example.gamebackend.api.dto.session.SessionFinalizeRequest;
import com.example.gamebackend.api.dto.session.SessionSaveMistakeRequest;
import com.example.gamebackend.api.dto.session.SessionStartRequest;
import com.example.gamebackend.security.CurrentUser;
import com.example.gamebackend.security.UserPrincipal;
import com.example.gamebackend.service.SessionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = "Sessions API")
@RestController
@RequestMapping("/api/sessions/")
@AllArgsConstructor
public class SessionController {

  private SessionService service;

  @PostMapping("create")
  @ApiOperation(value = "Create a new game session")
  public SessionCreateResponse create(@ApiIgnore @CurrentUser UserPrincipal userPrincipal) {
    return service.create(userPrincipal);
  }

  @PutMapping("start")
  @ApiOperation(value = "Start an existing game session")
  public ResponseEntity<String> start(@RequestBody SessionStartRequest request) {
    service.start(request.getSessionUuid());
    return ResponseEntity.ok("Started!");
  }

  @PutMapping("finalize")
  @ApiOperation(value = "Finalize an existing game session")
  public ResponseEntity<String> finalize(@RequestBody SessionFinalizeRequest request) {
    service.finalize(request.getSessionUuid());
    return ResponseEntity.ok("Finalized!");
  }

  @PostMapping("saveMistake")
  @ApiOperation(value = "Register a player's error in an existing game session")
  public ResponseEntity<String> saveMistake(@RequestBody SessionSaveMistakeRequest request) {
    service.saveMistake(request);
    return ResponseEntity.ok("Mistake saved!");
  }

}
