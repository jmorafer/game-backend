package com.example.gamebackend.controller;

import com.example.gamebackend.api.dto.user.UserIdentifierAvailabilityResponse;
import com.example.gamebackend.api.dto.user.UserMeResponse;
import com.example.gamebackend.api.dto.user.UserProfileResponse;
import com.example.gamebackend.security.CurrentUser;
import com.example.gamebackend.security.UserPrincipal;
import com.example.gamebackend.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = "Users API")
@RestController
@RequestMapping("/api/users/")
@AllArgsConstructor
public class UserController {

  private UserService service;

  @GetMapping("/me")
  @ApiOperation(
      value = "My profile",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE
  )
  private UserMeResponse me(@CurrentUser @ApiIgnore UserPrincipal userPrincipal) {
    return service.me(userPrincipal);
  }

  @GetMapping("/checkAvailabilityUsername")
  @ApiOperation(
      value = "Check availability of username",
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  private UserIdentifierAvailabilityResponse checkAvailabilityUsername(
      @ApiParam(value = "Username to verify", example = "johndoe") @RequestParam String username
  ) {
    return service.checkAvailabilityUsername(username);
  }

  @GetMapping("/checkAvailabilityEmail")
  @ApiOperation(
      value = "Check availability of email",
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  private UserIdentifierAvailabilityResponse checkAvailabilityEmail(
      @ApiParam(value = "Email to verify", example = "johndoe@mail.com") @RequestParam String email
  ) {
    return service.checkAvailabilityEmail(email);
  }

  @GetMapping("/{username}")
  @ApiOperation(
      value = "Retrieve user profile",
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  private UserProfileResponse searchProfile(
      @ApiParam(value = "Username to search", example = "johndoe") @PathVariable String username
  ) {
    return service.searchByUsername(username);
  }

}
