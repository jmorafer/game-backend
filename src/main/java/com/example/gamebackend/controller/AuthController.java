package com.example.gamebackend.controller;

import com.example.gamebackend.api.dto.auth.AuthLoginRequest;
import com.example.gamebackend.api.dto.auth.AuthLoginResponse;
import com.example.gamebackend.api.dto.auth.AuthSignUpRequest;
import com.example.gamebackend.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Authentication API")
@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

  private AuthService service;

  @PostMapping("/login")
  @ApiOperation(value = "Login")
  private AuthLoginResponse login(@RequestBody AuthLoginRequest request) {
    return service.login(request);
  }

  @GetMapping("/verify")
  @ApiOperation(value = "Verify account")
  private ResponseEntity<String> verify(@RequestParam String token) {
    service.verify(token);
    return ResponseEntity.ok("Verified!");
  }

  @PostMapping("/signup")
  @ApiOperation(value = "Sign up")
  private ResponseEntity<String> signup(@RequestBody AuthSignUpRequest request) {
    service.signup(request);
    return ResponseEntity.ok("Account created!");
  }

}
